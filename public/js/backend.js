$(function() {
	onload();

	function onload(){
		addBlogForm();
		blogTable();
		btn_modals();
		updateBlogForm();
	}
	function btn_modals(){
		$(document).on('click', '.btn-delete', function(){
			var val = $(this).attr('value'); 
			// AJAX request
			$.ajax({
			url: base_url + '/deleteBlog',
			type: 'POST',
			headers: {'X-CSRF-TOKEN': csrf_token},
			data: {val: val},
			success: function(response){ 
				  $('#deleteModal').modal('hide'); 
				  $('#deleteSuccess').modal('show'); 
				  $('#deleteSuccess').on('hidden.bs.modal', function () { 
				    location.reload();
				});
				}
			});
		});
		$(document).on('click', '.deleteModal', function(){
			var val = $(this).attr('value'); 
			console.log(val);
			$('.btn-delete').attr('value', val);
			$('#deleteModal').modal('show'); 
		});
		$(document).on('click', '.change-image', function(){
			$('.image-group').html('<label for="name" class="col-sm-4 control-label col-label">Image</label><div class="col-sm-12">\
			                        <input type="file" class="form-control" id="image" name="image">\
			                        <small class="error ur_image"></small>\
			                    </div>')
		});
		$(document).on('click', '.edit-btn', function(){
			var val = $(this).attr('value'); 
			// AJAX request
			$.ajax({
			url: base_url + '/getBlogInfo',
			type: 'get',
			data: {val: val},
			success: function(response){ 
				//put data in modal
				$('#title').val(response['title']);
				
				$('#image_realname').text(response['image_realname']);
				$('#description').val(response['description']);
				$('#blog_id').val(response['id']);
				$('.image-group').html('<label for="name" class="col-sm-4 control-label col-label">Image</label><br><img src="' + 'images/' + response['image'] + '" class="avatar " width="75" height="75"/>&nbsp;\
					<button class="change-image">Change</button>')
				  // Display Modal
				  $('#editFormModal').modal('show'); 
				}
			});
		});
	}
	function blogTable(){
		 $('#blogTable').DataTable( {
	        "ajax": base_url + '/blogList',
	        "columns": [
	            { "data": "title" },
	            { "data": "description" },
	            { "data": 'image',
	            	"render": function (data) {
				       return '<img src="' + 'images/' + data + '" class="avatar" width="50" height="50"/>';
				    }
	            },
	            { "data": 'id',
	            	"render": function (data) {
				       return '<button class="edit-btn btn-primary" value="'+data+'">Edit</button>\
				       <button class="deleteModal btn-warning" value="'+data+'">Delete</button>';
				    }
	            },
	        ]
	    } );
		 btn_modals
	}
	function addBlogForm(){
	    $("#addBlogForm").submit(function(e){
	    	e.preventDefault();
	    	var formData = new FormData(this);
	    $.ajax({
	        url: base_url + '/addBlogForm',
	        headers: {'X-CSRF-TOKEN': csrf_token},
	        data:  formData,
		    contentType: false,
		    type: 'POST',
		    cache: false,
		    processData:false,
	        success: function(data) {
	          console.log(data);
	          if(data.status == 'success'){
	          	$('#addBlogSuccess').modal('show');
	          	$('#addBlogSuccess').on('hidden.bs.modal', function () { 
				    location.reload();
				});
	          }else if(data.status == 'failed'){
	          	clearValidationAddForm();
	            $.each(data.errors,function(key, val){
	              $("small.error.r_" + key).text(val[0]);
	              });
	            }
	          }
	          
	        });
	      
	    });
	}

	function updateBlogForm(){
	    $("#updateBlogForm").submit(function(e){
	    	e.preventDefault();
	    	var formData = new FormData(this);
	    $.ajax({
	        url: base_url + '/updateBlogForm',
	        headers: {'X-CSRF-TOKEN': csrf_token},
	        data:  formData,
		    contentType: false,
		    type: 'POST',
		    cache: false,
		    processData:false,
	        success: function(data) {
	          console.log(data);
	          if(data.status == 'success'){
	          	$('#editFormModal').modal('hide');
	          	$('#updateBlogSuccess').modal('show');
	          	$('#updateBlogSuccess').on('hidden.bs.modal', function () { 
				    location.reload();
				});
	          }else if(data.status == 'failed'){
	          	clearValidationUpdateForm();
	            $.each(data.errors,function(key, val){
	              $("small.error.ur_" + key).text(val[0]);
	              });
	            }
	          }
	          
	        });
	      
	    });
	}

	function clearValidationAddForm(){
		$('small.error.r_title').text('');
		$('small.error.r_description').text('');
		$('small.error.r_image').text('');
	}
	function clearValidationUpdateForm(){
		$('small.error.ur_title').text('');
		$('small.error.ur_description').text('');
		$('small.error.ur_image').text('');
	}

});