<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::post('/addBlogForm','BlogController@addBlogForm');
route::post('/updateBlogForm','BlogController@updateBlogForm');
route::post('/deleteBlog','BlogController@deleteBlog');
route::get('/blogList','BlogController@blogList');
route::get('/getBlogInfo','BlogController@getBlogInfo');