<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    protected $table = 'blog';
    protected $fillable = ['title','description','image','image_realname','image_path','created_at','updated_at'];
    public $timestamps = false;
}
