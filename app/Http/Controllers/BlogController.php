<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogModel;
use Input;
use Validator;
use Response;

class BlogController extends Controller
{
    public function addBlogForm(Request $request){
    	$blogModel = new BlogModel;

    	$validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    
         if ($validator->fails())
        {
            $Response = [
                'status' => 'failed',
                'errors' => $validator->errors()
            ];            
            return Response::json($Response);
        }

         if($request->hasFile('image')) {

         	
	  
	        $imageName = time().'.'.$request->image->extension();  
	        $realname = $request->image->getClientOriginalName();
	        $image_path = public_path('images').$imageName;
	        $request->image->move(public_path('images'), $imageName);

            $save = $blogModel->insert([
	            'title'   => $request->title,
	            'description'   => $request->description,
	            'image'   => $imageName,
	            'image_realname'	=> $realname,
	            'image_path'	=> $image_path,
	            'created_at'	=> now()

	        ]);

	        $Response = [
                'status' => 'success',
                'errors' => $validator->errors()
            ];            
            return Response::json($Response);
        }else{
        	$Response = [
                'status' => 'failed'
            ];            
            return Response::json($Response);
        }
    }

    public function updateBlogForm(Request $request){
    	$blogModel = new BlogModel;

    	$validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required'
        ]);
    
         if ($validator->fails())
        {
            $Response = [
                'status' => 'failed',
                'errors' => $validator->errors()
            ];            
            return Response::json($Response);
        }

         if($request->hasFile('image')) {

         	
	  
	        $imageName = time().'.'.$request->image->extension();  
	        $realname = $request->image->getClientOriginalName();
	        $image_path = public_path('images').$imageName;
	        $request->image->move(public_path('images'), $imageName);

	        $save = $blogModel->where('id',$request->blog_id)->update([
                    'title'   => $request->title,
		            'description'   => $request->description,
		            'image'   => $imageName,
		            'image_realname'	=> $realname,
		            'image_path'	=> $image_path,
		            'updated_at'	=> now()
                ]);

	        $Response = [
                'status' => 'success',
                'errors' => $validator->errors()
            ];            
            return Response::json($Response);
        }else{
        	$save = $blogModel->where('id',$request->blog_id)->update([
                    'title'   => $request->title,
		            'description'   => $request->description,
		            'updated_at'	=> now()
                ]);

        	$Response = [
                'status' => 'success'
            ];            
            return Response::json($Response);
        }
    }
    
    public function blogList(){
    	$blogModel = new BlogModel;
    	$data['data'] = $blogModel->get()->toArray();
    	return $data;
    }

    public function getBlogInfo(Request $request){
    	$blogModel = new BlogModel;
    	$data = $blogModel->where('id',$request->val)->first();
    	return $data;
    }

    public function deleteBlog(Request $request){
    	$blogModel = new BlogModel;
    	$blog = $blogModel->where('id',$request->val)->delete();

    	if($blog){
    		$Response = [
                'status' => 'success'
            ];            
        	return Response::json($Response);
    	}else{
    		$Response = [
                'status' => 'failed'
            ];            
        return Response::json($Response);
    	}
    }

}
